package com.example.cocktailappv4.util

import android.net.Uri
import androidx.navigation.NavController
import com.example.cocktailappv4.R
import com.example.cocktailappv4.presentation.view.categories.CategoriesFragmentDirections
import com.example.cocktailappv4.presentation.view.drinkdetails.DrinkDetailsFragmentDirections
import com.example.cocktailappv4.presentation.view.drinksbycategory.DrinksByCategoryFragmentDirections

class BottomNavController(
    private val navController: NavController
) {

    fun setNavigationTab(fragmentId: Int): Boolean {
        // Seperate navigation by fragmentId

        when (fragmentId) {
            R.id.animals_btn -> setCocktailsTab()
            R.id.cocktails_btn -> setAnimalsTab()
        }
        return true
    }

    private fun setAnimalsTab() {
        when (navController.currentDestination?.label.toString()) {
            "TestFragment" -> {
                val uri = Uri.parse("android-app://com.example.app/category_fragment")
                navController.navigate(uri)
            }
        }
    }

    private fun setCocktailsTab() {
        when (navController.currentDestination?.label.toString()) {
            "DrinkDetailsFragment" -> {
                val action = DrinkDetailsFragmentDirections.actionDrinkDetailsFragmentToNavGraphAnimals()
                navController.navigate(action)
            }

            "DrinksByCategoryFragment" -> {
                val action = DrinksByCategoryFragmentDirections.actionDrinksByCategoryFragmentToNavGraphAnimals()
                navController.navigate(action)
            }

            "CategoryFragment" -> {
                val action = CategoriesFragmentDirections.actionCategoryFragmentToIncludedGraph()
                navController.navigate(action)
            }
        }
    }
}
