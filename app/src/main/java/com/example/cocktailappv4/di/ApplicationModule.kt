package com.example.cocktailappv4.di

import com.example.cocktailappv4.presentation.viewmodel.CategoryViewModel
import com.example.cocktailappv4.presentation.viewmodel.DrinkDetailsViewModel
import com.example.cocktailappv4.presentation.viewmodel.DrinksByCategoryViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val applicationModule = module() {

    viewModel { CategoryViewModel(get()) }

    viewModel { DrinksByCategoryViewModel(get()) }

    viewModel { DrinkDetailsViewModel(get()) }

}