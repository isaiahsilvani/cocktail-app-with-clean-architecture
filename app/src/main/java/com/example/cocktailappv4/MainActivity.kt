package com.example.cocktailappv4

import android.os.Bundle
import android.os.PersistableBundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.findNavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController

import com.example.cocktailappv4.databinding.ActivityMainBinding
import com.example.cocktailappv4.presentation.view.categories.CategoriesFragmentDirections
import com.example.cocktailappv4.util.BottomNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.play.core.splitinstall.SplitInstallManager
import com.google.android.play.core.splitinstall.SplitInstallManagerFactory
import com.google.android.play.core.splitinstall.SplitInstallRequest

private const val TAG = "MainActivity"
lateinit var bottomNavigationView: BottomNavigationView

class MainActivity : AppCompatActivity() {

    lateinit var splitInstallManager: SplitInstallManager
    var mySessionId: Int? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        bottomNavigationView = findViewById(R.id.bottomNav)
        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.navHost) as NavHostFragment
        val navController = navHostFragment.navController

        // Install the sonnofabitch
        splitInstallManager = SplitInstallManagerFactory.create(this)
        // If Feature Module not already installed, install it!!
        if (!splitInstallManager.installedModules.contains("animals_feature")) {
            Log.e(TAG, "Animals Feature not installed, install it now")
            val splitInstallRequest = SplitInstallRequest.newBuilder()
                .addModule("animals_feature")
                .build()
            splitInstallManager.startInstall(splitInstallRequest)
                .addOnSuccessListener { result ->
                    Log.e(TAG, "SUCCESS!! Feature Module installed")
                    this.mySessionId = result
                }
                .addOnFailureListener { e ->
                    Log.i(TAG, "Error installing feature module: $e")
                }
        } else {
            Log.e(TAG, "The animals_feature is already installed! :)")
        }
        val bottomNavigationView = findViewById<BottomNavigationView>(R.id.bottomNav)

        val bottomNavController = BottomNavController(navController)

        bottomNavigationView.setOnItemSelectedListener { item ->
            when(item.itemId) {
                R.id.animals_btn -> bottomNavController.setNavigationTab(R.id.animals_btn)
                R.id.cocktails_btn -> bottomNavController.setNavigationTab(R.id.cocktails_btn)
                else -> false
            }
        }

    }


}