package com.example.cocktailappv4.presentation.view.drinkdetails

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.bumptech.glide.Glide
import com.example.cocktailappv4.R
import com.example.cocktailappv4.databinding.FragmentDrinkBinding
import com.example.cocktailappv4.presentation.view.util.collectLatestLifecycleFlow
import com.example.cocktailappv4.presentation.viewmodel.DrinkDetailsViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

private const val TAG = "DrinkDetailsFragment"

class DrinkDetailsFragment : Fragment() {

    private lateinit var binding: FragmentDrinkBinding
    private val args by navArgs<DrinkDetailsFragmentArgs>()
    private val viewModel: DrinkDetailsViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getDrinkDetailsById(args.id)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDrinkBinding.inflate(layoutInflater)
        initObservers()
        return binding.root
    }
    // Set UI functionality in onResume for faster loading of UI
    override fun onResume() {
        super.onResume()
        binding.btnMainMenu.setOnClickListener {
            val action =
                DrinkDetailsFragmentDirections.actionDrinkDetailsFragmentToCategoryFragment()
            findNavController().navigate(action)
        }
    }

    private fun initObservers() {
        with(binding) {
            collectLatestLifecycleFlow(viewModel.drinkDetailsState) { state ->
                Log.e(TAG, "The state of DrinkDetailsFragment is:  $state")
                with(state) {
                    // Loading.....
                    progressBar.isVisible = state.isLoading
                    Log.e(TAG, "The state is loading: ${progressBar.isVisible}")
                    tvDrinkName.text = drink?.strDrink

                    Glide.with(this@DrinkDetailsFragment)
                        .load(drink?.strDrinkThumb)
                        .into(imgDrink)

                    tvInstructions.text = if (drink?.strInstructions?.isEmpty() == true) {
                        // todo ask your boys about this one
                        context?.getString(R.string.no_instructions)
                    } else {
                        drink?.strInstructions
                    }


                    // set the Go Back button to the state's category
                    btnGoBack.setOnClickListener {
                        drink?.strCategory?.let {
                            val action =
                                DrinkDetailsFragmentDirections
                                    .actionDrinkDetailsFragmentToDrinksByCategoryFragment(it)
                            findNavController().navigate(action)
                        }
                    }
                }
            }

        }
    }
}