package com.example.cocktailappv4.presentation.view.drinksbycategory

import com.example.domain.models.Drink

data class DrinksByCategoryState(
    val drinks: List<Drink> = listOf(),
    val category: String? = null,
    var isLoading: Boolean = false
)