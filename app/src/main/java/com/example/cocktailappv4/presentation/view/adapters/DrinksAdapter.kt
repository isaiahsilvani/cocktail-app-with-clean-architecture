package com.example.cocktailappv4.presentation.view.adapters

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.cocktailappv4.databinding.ItemDrinkViewBinding
import com.example.cocktailappv4.presentation.view.drinksbycategory.DrinksByCategoryFragmentDirections
import com.example.domain.models.Drink

private const val TAG = "DrinksAdapter"

class DrinksAdapter(
    val context: Context
) : RecyclerView.Adapter<DrinksAdapter.DrinkViewHolder>() {

    var drinkList: List<Drink> = listOf()

    inner class DrinkViewHolder(private val binding: ItemDrinkViewBinding) :
        RecyclerView.ViewHolder(binding.root) {
        var drinkName: TextView = binding.tvDrink
        var drinkImg: ImageView = binding.imgDrink
        var wholeItem: ConstraintLayout = binding.layoutItem
        fun displayImage(image: String?) {
            Glide.with(context)
                .load(image)
                .into(drinkImg)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DrinkViewHolder {
        return DrinkViewHolder(
            ItemDrinkViewBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: DrinksAdapter.DrinkViewHolder, position: Int) {
        with(holder) {
            drinkList[position].run {
                drinkName.text = strDrink
                displayImage(strDrinkThumb)
                val elements = listOf(drinkName, drinkImg, wholeItem)
                for (element in elements) {
                    element.setOnClickListener {
                        // TODO - Put Navigation function in the fragment
                        idDrink.let {
                            val action = DrinksByCategoryFragmentDirections
                                .actionDrinksByCategoryFragmentToDrinkDetailsFragment(it)

                            Navigation.findNavController(holder.itemView)
                                .navigate(action)
                        }
                    }
                }
            }
        }
    }

    override fun getItemCount(): Int {
        return drinkList.size
    }

    fun setList(list: List<Drink>) {
        val oldSize = drinkList.size
        drinkList = list
        Log.e("TAG", drinkList.toString())
        notifyItemRangeChanged(oldSize, drinkList.size)
    }

}