package com.example.cocktailappv4.presentation.view.drinkdetails

import com.example.domain.models.Drink

data class DrinkDetailState(
    val drink: Drink? = null,
    var isLoading: Boolean = false
)