package com.example.cocktailappv4.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailappv4.presentation.view.drinksbycategory.DrinksByCategoryState
import com.example.domain.usecases.GetDrinksByCategoryUseCase
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

private const val TAG = "DrinksByCatVM"

class DrinksByCategoryViewModel(
    private val drinksByCategoryUseCase: GetDrinksByCategoryUseCase
) : ViewModel() {

    private val _drinksByCategoryState: MutableStateFlow<DrinksByCategoryState> = MutableStateFlow(
        DrinksByCategoryState()
    )
    val drinksByCategoryState: StateFlow<DrinksByCategoryState> get() = _drinksByCategoryState.asStateFlow()

    fun getDrinksByCategory(category: String) {
        viewModelScope.launch {
            drinksByCategoryUseCase(category).catch { e ->
            }.collect { drinks ->
                val drinksFromRepo = drinks.getOrNull()
                Log.e(TAG, "There are ${drinksFromRepo?.size}  drinks in the flow")
                if (drinks.isSuccess && drinksFromRepo != null) {
                    _drinksByCategoryState.value = DrinksByCategoryState(drinksFromRepo)
                }
            }
        }
    }

}