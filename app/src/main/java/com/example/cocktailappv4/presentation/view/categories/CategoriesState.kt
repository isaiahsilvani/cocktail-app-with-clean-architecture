package com.example.cocktailappv4.presentation.view.categories

import com.example.domain.models.Categories

data class CategoriesState(
    val categories: Categories = Categories(listOf())
)