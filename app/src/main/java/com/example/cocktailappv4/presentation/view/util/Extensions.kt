package com.example.cocktailappv4.presentation.view.util

import android.content.res.Configuration
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collectLatest

private const val TAG = "collectLatestLifecycleFlow()"

fun <T> Fragment.collectLatestLifecycleFlow(flow: Flow<T>, collect: suspend (T) -> Unit) =
    lifecycleScope.launchWhenStarted {
        flow.collectLatest(collect)
    }

fun RecyclerView.configureRecyclerViewLayout(orientation: Int) {
    layoutManager = if (orientation == Configuration.ORIENTATION_LANDSCAPE)
        GridLayoutManager(context, 2) else LinearLayoutManager(context)
}