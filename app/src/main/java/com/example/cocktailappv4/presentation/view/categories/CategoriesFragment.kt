package com.example.cocktailappv4.presentation.view.categories

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.cocktailappv4.databinding.FragmentCategoriesBinding
import com.example.cocktailappv4.presentation.view.adapters.CategoryAdapter
import com.example.cocktailappv4.presentation.view.util.collectLatestLifecycleFlow
import com.example.cocktailappv4.presentation.view.util.configureRecyclerViewLayout
import com.example.cocktailappv4.presentation.viewmodel.CategoryViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

private const val TAG = "CategoriesFragment"

class CategoriesFragment : Fragment() {

    private lateinit var binding: FragmentCategoriesBinding
    private val viewModel: CategoryViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getCategories()
    }


    private val adapter by lazy {
        CategoryAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        Log.e(TAG, "onCreateView() CategoriesFragment")
        binding = FragmentCategoriesBinding.inflate(layoutInflater)
        with(binding) {
            rvDrinksByCategory.run {
                adapter = this@CategoriesFragment.adapter
                configureRecyclerViewLayout(resources.configuration.orientation)
            }
        }
        initObservers()
        return binding.root
    }

    private fun initObservers() {
        collectLatestLifecycleFlow(viewModel.categoriesState) { state ->
            Log.e(TAG, "SANITY CHECK WASSUP MANNNNEEEEEE!!!!")
            Log.e(TAG, "categoriesState drinks list is empty: ${state.categories.drinks.isEmpty()}")
            adapter.setData(state.categories.drinks)
            binding.progressBar.isVisible = state.categories.drinks.isEmpty()
        }
    }
}