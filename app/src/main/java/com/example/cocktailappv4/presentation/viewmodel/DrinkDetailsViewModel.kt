package com.example.cocktailappv4.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailappv4.presentation.view.drinkdetails.DrinkDetailState
import com.example.domain.usecases.GetDrinkDetailsByIdUseCase
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

private const val TAG = "DrinkDeetsViewModel"

class DrinkDetailsViewModel(
    private val drinkDetailsUseCase: GetDrinkDetailsByIdUseCase
) : ViewModel() {

    private val _drinkDetailsState: MutableStateFlow<DrinkDetailState> = MutableStateFlow(
        DrinkDetailState()
    )
    val drinkDetailsState: StateFlow<DrinkDetailState> get() = _drinkDetailsState.asStateFlow()

    fun getDrinkDetailsById(id: String) {
        viewModelScope.launch {
            with(_drinkDetailsState) {
                value = DrinkDetailState(isLoading = true)
                val result = drinkDetailsUseCase.execute(id)
                value = DrinkDetailState(result, isLoading = false)
            }
        }
    }
}