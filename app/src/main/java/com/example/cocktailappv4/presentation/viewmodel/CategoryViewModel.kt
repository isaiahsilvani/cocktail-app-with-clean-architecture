package com.example.cocktailappv4.presentation.viewmodel

import android.util.Log
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.cocktailappv4.presentation.view.categories.CategoriesState
import com.example.domain.models.Categories
import com.example.domain.usecases.GetCategoriesUseCase
import kotlinx.coroutines.awaitCancellation
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch

private const val TAG = "CategoryViewModel"

class CategoryViewModel(
    private val categoriesUseCase: GetCategoriesUseCase,
) : ViewModel() {

    private val _categoriesState: MutableStateFlow<CategoriesState> = MutableStateFlow(
        CategoriesState()
    )
    val categoriesState: StateFlow<CategoriesState> get() = _categoriesState.asStateFlow()

    // Trigger the repo getting categories from the data layer, and collect the data
    fun getCategories() {
        viewModelScope.launch {
            categoriesUseCase().catch { e ->
            }.collectLatest { categoriesResult ->
                    val categories = categoriesResult.getOrNull()
                    if (categoriesResult.isSuccess && categories != null) {
                        _categoriesState.value = CategoriesState(categories)
                    }
                }

        }
    }
}