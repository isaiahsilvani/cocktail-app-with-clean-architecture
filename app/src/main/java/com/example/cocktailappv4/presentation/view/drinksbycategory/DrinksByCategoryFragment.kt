package com.example.cocktailappv4.presentation.view.drinksbycategory

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import com.example.cocktailappv4.databinding.FragmentDrinksByCategoryBinding
import com.example.cocktailappv4.presentation.view.adapters.DrinksAdapter
import com.example.cocktailappv4.presentation.view.util.collectLatestLifecycleFlow
import com.example.cocktailappv4.presentation.view.util.configureRecyclerViewLayout
import com.example.cocktailappv4.presentation.viewmodel.DrinksByCategoryViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

private const val TAG = "DrinksByCatFrag"

class DrinksByCategoryFragment : Fragment() {

    private lateinit var binding: FragmentDrinksByCategoryBinding
    private val args by navArgs<DrinksByCategoryFragmentArgs>()
    private val viewModel: DrinksByCategoryViewModel by viewModel()

    private val drinksAdapter by lazy {
        DrinksAdapter(this@DrinksByCategoryFragment.requireContext())
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.getDrinksByCategory(args.category)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentDrinksByCategoryBinding.inflate(layoutInflater)
        Log.e(TAG, "onCreateView -- The category is ${args.category}")
        // Set up Recycler View
        with(binding) {
            rvDrinksByCategory.run {
                rvDrinksByCategory.adapter = drinksAdapter
                tvCategory.text = args.category
                configureRecyclerViewLayout(resources.configuration.orientation)
            }
        }
        initObservers()
        return binding.root
    }

    private fun initObservers() {
        collectLatestLifecycleFlow(viewModel.drinksByCategoryState) { state ->
            Log.e(TAG,"Listening. The state is ${state.drinks}. Its empty: ${state.isLoading}")
            drinksAdapter.setList(state.drinks)
            binding.progressBar.isVisible = state.drinks.isEmpty()
        }
    }

}
