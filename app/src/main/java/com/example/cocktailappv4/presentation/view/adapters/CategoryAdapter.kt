package com.example.cocktailappv4.presentation.view.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.navigation.Navigation
import androidx.recyclerview.widget.RecyclerView
import com.example.cocktailappv4.R
import com.example.cocktailappv4.presentation.view.categories.CategoriesFragmentDirections
import com.example.domain.models.Category
import com.example.domain.models.Drink

private const val TAG = "CategoryAdapter"

class CategoryAdapter : RecyclerView.Adapter<CategoryAdapter.CategoryViewHolder>() {

    var categoryList: List<Category> = listOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoryViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.item_category, parent, false)
        return CategoryViewHolder(v)
    }

    override fun onBindViewHolder(holder: CategoryViewHolder, position: Int) {
        with(holder.categoryName) {
            text = categoryList[position].strCategory
            // TODO - Put Navigation function in the fragment
            setOnClickListener {
                val action = CategoriesFragmentDirections.actionCategoryFragmentToDrinksByCategoryFragment(
                    text.toString()
                )
                val navController = Navigation.findNavController(holder.itemView)
                navController.navigate(action)
            }
        }
    }

    override fun getItemCount(): Int {
        return categoryList.size
    }

    inner class CategoryViewHolder(categoryView: View) : RecyclerView.ViewHolder(categoryView) {
        var categoryName: TextView

        init {
            categoryName = categoryView.findViewById(R.id.tvCategoryItem)
        }
    }

    fun setData(list: List<Category>) {
        categoryList = list
        notifyItemRangeChanged(0, categoryList.size)
    }
}