package com.example.cocktailappv4

import android.app.Application
import android.content.Context
import com.example.cocktailappv4.di.applicationModule
import com.example.data.di.dataModule
import com.example.domain.di.domainModule
import com.google.android.play.core.splitcompat.SplitCompat
import com.google.android.play.core.splitcompat.SplitCompatApplication
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin

class CocktailApplication : SplitCompatApplication() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@CocktailApplication)
            modules(
                dataModule,
                domainModule,
                applicationModule
            )
        }
    }

    override fun attachBaseContext(base: Context?) {
        super.attachBaseContext(base)
        SplitCompat.install(this)
    }

}