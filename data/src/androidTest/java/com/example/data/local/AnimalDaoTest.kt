package com.example.data.local

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import com.example.data.getOrAwaitValue
import com.example.domain.models.Animal
import com.example.domain.models.AnimalType
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

// JUnit is a library that is used to test Java or Kotlin code (any code that runs on JVM).
// Here we're not in a plain JVM enviroment because we're in an android environment since we
// need the Android Emulator. @RunWith will run on the emulator, and tells JUnit these are
// instrumented tests
@RunWith(AndroidJUnit4::class)
@SmallTest
class AnimalDaoTest {
    @get:Rule
    val instantTaskExecutor = InstantTaskExecutorRule()

    private lateinit var database: CocktailDB
    private lateinit var dao: AnimalDao

    @Before
    fun setup() {
        // not a real database. Holds DB in RAM, not persistent storage
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            CocktailDB::class.java
        ).allowMainThreadQueries().build()
        dao = database.animalDao()
    }

    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun getAnimalsByTypeTest() = runBlocking {
        val animals = listOf(
            Animal("image1", AnimalType.BIRD),
            Animal("image2", AnimalType.DOG),
            Animal("image3", AnimalType.DOG),
        )
        dao.insertAnimal(*animals.toTypedArray())
        val dogs = dao.getAnimalsByType(AnimalType.DOG)
        dogs.forEach { dog ->
            if (dog.type != AnimalType.DOG) {
                assertThat(dog.type).isEqualTo(AnimalType.DOG)
            } else if (dogs.indexOf(dog) == dogs.size - 1) {
                assertThat(dog.type).isEqualTo(AnimalType.DOG)
            }
        }
    }

    @Test
    fun insertAnimalsTest(): Unit = runBlocking {
        val animals = listOf(
            Animal("image", AnimalType.BIRD),
            Animal("image", AnimalType.DOG),
        )
        dao.insertAnimal(*animals.toTypedArray())
        val allAnimals = dao.getAllAnimals().getOrAwaitValue()
        assertThat(allAnimals.containsAll(animals))
    }

}