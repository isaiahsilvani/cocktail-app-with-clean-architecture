package com.example.data.local

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import app.cash.turbine.test
import com.example.domain.models.Category
import com.example.domain.models.Drink
import com.google.common.truth.Truth.assertThat
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
@SmallTest
class CocktailDaoTest {
    @get:Rule
    val instantTaskExecutor = InstantTaskExecutorRule()

    private lateinit var database: CocktailDB
    private lateinit var dao: CocktailDao

    @Before
    fun setup() {
        // not a real database. Holds DB in RAM, not persistent storage
        database = Room.inMemoryDatabaseBuilder(
            ApplicationProvider.getApplicationContext(),
            CocktailDB::class.java
        ).allowMainThreadQueries().build()
        dao = database.cocktailDao()
    }

    @After
    fun teardown() {
        database.close()
    }

    @Test
    fun insertCategories() = runBlocking {
        val categories = listOf(Category("pie"), Category("potatoe"))
        dao.insertCategories(categories)
        val allCategories = dao.getCategories()
        allCategories.test {
            val emission = awaitItem()
            assertThat(emission.containsAll(categories))
        }
    }

    @Test
    fun insertDrink() = runBlocking {
        val drink = Drink(strDrink = "Sprite", idDrink = "1")
        dao.insertDrink(drink)
        val drinkFromDB = dao.getSingleDrink("1")
        assertThat(drink).isEqualTo(drinkFromDB)
    }

    @Test
    fun insertDrinks() = runBlocking {
        val drinks = listOf(
            Drink(strDrink = "Sprite", strCategory = "test"),
            Drink(strDrink = "Coke", strCategory = "test")
        )
        dao.insertDrinks(drinks)
        val drinksFromDB = dao.getDrinksByCategory("test")
        drinksFromDB.test {
            val emission = awaitItem()
            assertThat(emission.containsAll(drinks))
        }
    }
}