package com.example.data.remote.services

import retrofit2.http.GET
import retrofit2.http.Query

interface AnimalService{

    companion object {
        const val BASE_URL = "https://shibe.online"
        private const val ENDPOINT_DOG = "/api/shibes"
        private const val ENDPOINT_CAT = "/api/cats"
        private const val ENDPOINT_BIRD = "/api/birds"
    }

    @GET(ENDPOINT_DOG)
    suspend fun getDogs(@Query("count") count: Int = 10): List<String>

    @GET(ENDPOINT_CAT)
    suspend fun getCats(@Query("count") count: Int = 10): List<String>

    @GET(ENDPOINT_BIRD)
    suspend fun getBirds(@Query("count") count: Int = 10): List<String>
}