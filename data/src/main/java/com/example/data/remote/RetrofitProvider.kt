package com.example.data.remote

import com.example.data.remote.services.AnimalService
import com.example.data.remote.services.CocktailService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.create

object RetrofitProvider {

    private val retrofitObject: Retrofit.Builder = Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create())

    fun getCocktailService(): CocktailService =
        retrofitObject.baseUrl(CocktailService.BASE_URL)
            .build()
            .create()

    fun getAnimalService(): AnimalService = Retrofit.Builder()
        .baseUrl(AnimalService.BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()
        .create()

}