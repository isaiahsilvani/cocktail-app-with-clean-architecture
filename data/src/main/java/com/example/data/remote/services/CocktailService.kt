package com.example.data.remote.services

import com.example.domain.models.Categories
import com.example.domain.models.Drinks
import retrofit2.http.GET
import retrofit2.http.Query

interface CocktailService {

    companion object {
        const val BASE_URL = "https://www.thecocktaildb.com"
        private const val CATEGORYLIST_ENDPOINT = "/api/json/v1/1/list.php?c=list"
        private const val DRINKS_ENDPOINT = "/api/json/v1/1/filter.php"
        private const val DRINK_ENDPOINT = "/api/json/v1/1/lookup.php"

    }

    @GET(CATEGORYLIST_ENDPOINT)
    suspend fun getCategories(): Categories

    @GET(DRINKS_ENDPOINT)
    suspend fun getDrinks(@Query("c") c: String): Drinks

    @GET(DRINK_ENDPOINT)
    suspend fun getDrinkDetails(@Query("i") i: String): Drinks
}