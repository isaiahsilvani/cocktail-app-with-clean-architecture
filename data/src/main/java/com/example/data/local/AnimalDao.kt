package com.example.data.local

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.domain.models.Animal
import com.example.domain.models.AnimalType

@Dao
interface AnimalDao {
    // Get the animals
    @Query("SELECT * FROM animal_table WHERE type = :animalType")
    suspend fun getAnimalsByType(animalType: AnimalType): List<Animal>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertAnimal(vararg animal: Animal)

    @Query("SELECT * FROM animal_table")
    fun getAllAnimals(): LiveData<List<Animal>>
}