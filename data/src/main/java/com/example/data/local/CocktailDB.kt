package com.example.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.domain.models.Animal
import com.example.domain.models.Category
import com.example.domain.models.Drink

@Database(entities = [Category::class, Drink::class, Animal::class], version = 1, exportSchema = false)
abstract class CocktailDB : RoomDatabase() {

    abstract fun cocktailDao(): CocktailDao
    abstract fun animalDao(): AnimalDao

    companion object {
        private const val DATABASE_NAME = "cocktail.db"

        @Volatile
        private var instance: CocktailDB? = null

        fun getInstance(context: Context): CocktailDB {
            return instance ?: synchronized(this) {
                instance ?: buildDatabase(context).also {
                    instance = it
                }
            }
        }

        private fun buildDatabase(context: Context): CocktailDB {
            return Room
                .databaseBuilder(context, CocktailDB::class.java, DATABASE_NAME)
                .build()
        }
    }

}