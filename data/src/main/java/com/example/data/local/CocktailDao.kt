package com.example.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.example.domain.models.Categories
import com.example.domain.models.Category
import com.example.domain.models.Drink
import kotlinx.coroutines.flow.Flow

@Dao
interface CocktailDao {

    // Insert ALL categories
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertCategories(categories: List<Category>)

    // Retrieve ALL Categories
    @Query("SELECT * FROM category_table")
    fun getCategories(): Flow<List<Category>>

    // Insert specific drink
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDrink(drink: Drink)

    // Retrieve specific SINGLE drink by ID
    @Query("SELECT * FROM drink_table WHERE idDrink = :id")
    suspend fun getSingleDrink(id: String): Drink

    // Retrieve multiple drinks WHERE category
    @Query("SELECT * FROM drink_table WHERE strCategory = :category")
    fun getDrinksByCategory(category: String): Flow<List<Drink>>

    // Insert multiple drinks
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertDrinks(drinks: List<Drink>)

}