package com.example.data

import android.util.Log
import com.example.data.local.AnimalDao
import com.example.data.local.CocktailDao
import com.example.data.remote.services.AnimalService
import com.example.data.remote.services.CocktailService
import com.example.data.util.checkDatabaseForDrink
import com.example.domain.models.*
import com.example.domain.repositories.CocktailRepo
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

private const val TAG = "CocktailRepository"

class CocktailRepository(
    internal val cocktailService: CocktailService,
    internal val animalService: AnimalService,
    internal val cocktailDao: CocktailDao,
    internal val animalDao: AnimalDao,
    private val scope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)
) : CocktailRepo {

    override suspend fun getCategories(): Flow<Categories> = callbackFlow {
        getCategoriesFromDatabase().collectLatest { categoriesFromDB ->
            val localCategories = categoriesFromDB.getOrNull()
            Log.e(TAG, "Local categories is: $localCategories. Is it nullOrEmpty?: ${localCategories.isNullOrEmpty()}")
            if (localCategories.isNullOrEmpty()) {
                val categoriesResponse = async { cocktailService.getCategories() }
                scope.launch {
                    cocktailDao.insertCategories(categoriesResponse.await().drinks)
                    Log.e(TAG, "The drinks from API are ${categoriesResponse.await()}")
                }
                send(categoriesResponse.await())
            } else {
                send(convertDBCategoriesToLocalCategories(localCategories))
            }
        }
    }

    override suspend fun getDrinksByCategory(category: String): Flow<List<Drink>> = callbackFlow {
        getDrinksByCategoryFromDB(category).cancellable().collect { drinksFromDB ->
            val localDrinks = drinksFromDB.getOrNull()
            Log.e(TAG, " ---- getDrinksByCategory ---- drinksFromDb are: ${localDrinks?.size}}")
            if (localDrinks.isNullOrEmpty()) {
                val drinksResponse = async { cocktailService.getDrinks(category).drinks }
                val drinks = drinksResponse.await()
                // Drinks are stored to database WHILE drinks are sent back to UI, at the SAME TIME!!! :O
                val job = scope.launch {
                    Log.e(TAG, "---- inserting these drinks from API into db: ${drinks.size}")
                    cocktailDao.insertDrinks(drinks)
                }
                Log.e(TAG, "Wait for database job to be done. Block current thread")
                job.join()
                send(drinks)
            } else {
                Log.e(TAG, "Drinks are not empty!! It's ${localDrinks.size} Put it back in database")
                send(localDrinks)
            }
            this.cancel()
        }
        Log.e(TAG, "------ END OF DRINKS CATEGORY --------")
    }

    override suspend fun getDrinkDetailsById(id: String): Drink =
        withContext(Dispatchers.IO) {
            return@withContext checkDatabaseForDrink(id)
        }

    override suspend fun getAnimalsByType(animalType: AnimalType): List<Animal> = withContext(Dispatchers.IO){
        val localAnimals = animalDao.getAnimalsByType(animalType)
        Log.e(TAG, "LocalAnimals is $localAnimals. Is it empty? ${localAnimals.isEmpty()}")
        if (localAnimals.isEmpty()) {
            val networkAnimals = async { animalService.getBirds(10).map {
                Animal(image = it, type = animalType)
            } }
            Log.e(TAG, "The animals from network are ${networkAnimals.await()}")
            animalDao.insertAnimal(*networkAnimals.await().toTypedArray())
            return@withContext networkAnimals.await()
        } else {
            Log.e(TAG, "There are already animals in database!")
            return@withContext localAnimals
        }
    }

    private suspend fun getDrinksByCategoryFromDB(category: String): Flow<Result<List<Drink>>> =
        callbackFlow {
            Log.e(TAG, "-------------- getDrinksByCategoryFromDB ------------- ")
            cocktailDao.getDrinksByCategory(category)
                .catch() { e ->
                    Log.e(TAG, "There was an errpr: $e")
                    send(Result.failure(e))
                    // The issue is that when a single drink is selected, its full details are stored in the database
                    // And as a result the drink is the latest thing in state. We need to collect ALL
                    // the drinks from the database, not just the latest drink to have an update
                }.collect { drinks ->
                    Log.e(
                        "BUG",
                        "--- SUCCESS!! The drinks from the database are ${drinks.size}. Is it empty?: ${drinks.isEmpty()}"
                    )
                    send(Result.success(drinks))
                    this.cancel()
                }

        }

    private suspend fun getCategoriesFromDatabase(): Flow<Result<List<Category>>> = callbackFlow {
        cocktailDao.getCategories()
            .catch() { e ->
                Log.e(TAG, "There was an errpr: $e")
                send(Result.failure(e))
            }.collectLatest { categories ->
                send(Result.success(categories))
            }
    }

    private fun convertDBCategoriesToLocalCategories(localCategories: List<Category>?): Categories {
        val categories = mutableListOf<Category>()
        localCategories?.forEach {
            categories.add(it)
        }
        return Categories(drinks = categories)
    }

}