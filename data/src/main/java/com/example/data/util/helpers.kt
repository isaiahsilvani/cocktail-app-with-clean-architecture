package com.example.data.util

import android.util.Log
import com.example.data.CocktailRepository
import com.example.domain.models.Drink

private const val TAG = "checkDBForDrink"

suspend fun CocktailRepository.checkDatabaseForDrink(id: String): Drink {
    Log.e(TAG, "------- call checkDatabaseForDrink() ------- ")
    var drink = this.cocktailDao.getSingleDrink(id)
    Log.e(TAG, "drink $id is in database: ${!drink?.strDrink.isNullOrBlank()}")
    if (drink?.strInstructions == null) {
        drink = cocktailService.getDrinkDetails(id).drinks[0]
        Log.e(TAG, "drink $id is null, better fetch it from api")
        cocktailDao.insertDrink(drink)
        Log.e(TAG, "drink $id inserted into room database!! holy cow dude")
        Log.e(TAG, " ------- end checkDatabaseForDrink() function call ------- ")
        return drink
    } else {
        Log.e(TAG, "drink $id is not null. Just return it.")
        Log.e(TAG, " ------- end checkDatabaseForDrink() function call ------- ")
        return drink
    }
}