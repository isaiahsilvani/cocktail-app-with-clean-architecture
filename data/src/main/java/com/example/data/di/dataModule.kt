package com.example.data.di

import com.example.data.CocktailRepository
import com.example.data.local.CocktailDB
import com.example.data.remote.RetrofitProvider
import com.example.domain.repositories.CocktailRepo
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val dataModule = module {

    single {
        CocktailDB.getInstance(androidContext()).cocktailDao()
    }

    single {
        CocktailDB.getInstance(androidContext()).animalDao()
    }

    single {
        RetrofitProvider.getCocktailService()
    }

    single {
        RetrofitProvider.getAnimalService()
    }

    single<CocktailRepo> { CocktailRepository(get(), get(), get(), get()) }

}