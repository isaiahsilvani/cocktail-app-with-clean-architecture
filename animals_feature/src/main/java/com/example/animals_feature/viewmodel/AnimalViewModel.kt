package com.example.animals_feature.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.animals_feature.view.BirdsFragment.BirdState
import com.example.domain.models.AnimalType
import com.example.domain.usecases.GetAnimalsUseCase
import kotlinx.coroutines.launch

class AnimalViewModel(
    private val animalsUseCase: GetAnimalsUseCase
) : ViewModel() {

    private val _birdState: MutableLiveData<BirdState> = MutableLiveData(BirdState())
    val birdState: LiveData<BirdState> get() = _birdState



    fun getBirds() {
        Log.e("GET", "GET THE DAMN BIRDS DAWG")
        viewModelScope.launch {
            val result = animalsUseCase.getAnimals(AnimalType.BIRD)
            Log.e("AVM", "THe bird result is $result")
            _birdState.value = BirdState(birds = result)
        }
    }

}