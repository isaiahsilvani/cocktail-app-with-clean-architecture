package com.example.animals_feature.view

import android.util.Log
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import coil.load
import com.example.animals_feature.databinding.ItemAnimalBinding
import com.example.domain.models.Animal

class AnimalAdapter : RecyclerView.Adapter<AnimalAdapter.AnimalViewHolder>() {

    private var animalList: MutableList<Animal> = mutableListOf()

    inner class AnimalViewHolder(val binding: ItemAnimalBinding): RecyclerView.ViewHolder(binding.root) {
        fun displayImage(image: String) {
            binding.imgAnimal.load(image)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimalViewHolder {
        return AnimalViewHolder(
            ItemAnimalBinding.inflate(LayoutInflater.from(parent.context))
        )
    }

    override fun onBindViewHolder(holder: AnimalViewHolder, position: Int) {
        holder.displayImage(animalList[position].image)
    }

    override fun getItemCount(): Int = animalList.size

    fun updateList(list: List<Animal>) {
        Log.e("Adapter", list.toString())
        Log.e("UPDATE", list.toString())
        val oldSize = animalList.size
        animalList.clear()
        notifyItemRangeChanged(0, oldSize)
        animalList.addAll(list)
        notifyItemRangeChanged(0, animalList.size)
    }

}