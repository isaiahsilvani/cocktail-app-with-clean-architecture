package com.example.animals_feature.view.BirdsFragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.example.animals_feature.R
import com.example.animals_feature.databinding.FragmentTestBinding
import com.example.animals_feature.view.AnimalAdapter
import com.example.animals_feature.view.loadModules
import com.example.animals_feature.viewmodel.AnimalViewModel
import com.example.cocktailappv4.presentation.viewmodel.CategoryViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel


private val loadFeature by lazy { loadModules() }
private fun injectFeature() = loadFeature

class BirdsFragment: Fragment() {

    lateinit var binding: FragmentTestBinding


    private val animalAdapter by lazy {
        AnimalAdapter()
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        injectFeature()
        val viewModel: AnimalViewModel by viewModel()
        viewModel.getBirds()
        initObservers(viewModel)
        binding = FragmentTestBinding.inflate(layoutInflater)
        with(binding) {
            rvBirds.adapter = animalAdapter
            rvBirds.layoutManager = GridLayoutManager(requireContext(), 2)

        }
        return binding.root
    }

    private fun initObservers(viewModel: AnimalViewModel) {
        viewModel.birdState.observe(viewLifecycleOwner) { state ->
            Log.e("TAGG", "The bird state is ${state.birds}")
            animalAdapter.updateList(state.birds)

        }
    }
}