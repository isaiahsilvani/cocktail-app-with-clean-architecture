package com.example.animals_feature.view.BirdsFragment

import com.example.domain.models.Animal

data class BirdState(
    val isLoading: Boolean = false,
    val birds: List<Animal> = emptyList()
)