package com.example.animals_feature.view

import android.content.Context
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import com.example.animals_feature.R
import com.example.animals_feature.di.animalsModule
import com.example.cocktailappv4.di.applicationModule
import com.example.data.di.dataModule
import com.example.domain.di.domainModule
import com.google.android.play.core.splitcompat.SplitCompat
import org.koin.core.context.loadKoinModules

private val loadFeature by lazy { loadModules() }
private fun injectFeature() = loadFeature

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(newBase)
        SplitCompat.install(this)
    }

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        injectFeature()
    }

}
// Load the Koin modules WITH the animalsModule included!!!
fun loadModules() {
    loadKoinModules(
        listOf(
            dataModule,
            domainModule,
            applicationModule,
            animalsModule,
        )
    )
}