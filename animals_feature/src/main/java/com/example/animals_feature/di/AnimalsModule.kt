package com.example.animals_feature.di

import com.example.animals_feature.viewmodel.AnimalViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val animalsModule = module() {

    viewModel { AnimalViewModel(get()) }

}