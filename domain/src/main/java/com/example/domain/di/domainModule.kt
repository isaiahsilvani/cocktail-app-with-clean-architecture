package com.example.domain.di

import com.example.domain.usecases.GetAnimalsUseCase
import com.example.domain.usecases.GetCategoriesUseCase
import com.example.domain.usecases.GetDrinkDetailsByIdUseCase
import com.example.domain.usecases.GetDrinksByCategoryUseCase
import org.koin.dsl.module

val domainModule = module {

    single { GetCategoriesUseCase(get()) }

    single { GetDrinksByCategoryUseCase(get()) }

    single { GetDrinkDetailsByIdUseCase(get()) }

    single { GetAnimalsUseCase(get()) }

}