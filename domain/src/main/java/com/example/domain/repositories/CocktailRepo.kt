package com.example.domain.repositories

import com.example.domain.models.Animal
import com.example.domain.models.AnimalType
import com.example.domain.models.Categories
import com.example.domain.models.Drink
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharedFlow


interface CocktailRepo {

    suspend fun getCategories(): Flow<Categories>

    suspend fun getDrinksByCategory(category: String): Flow<List<Drink>>

    suspend fun getDrinkDetailsById(id: String): Drink

    // Animals Section
    suspend fun getAnimalsByType(animalType: AnimalType): List<Animal>

}