package com.example.domain.usecases

import android.util.Log
import com.example.domain.models.Animal
import com.example.domain.models.AnimalType
import com.example.domain.models.Categories
import com.example.domain.repositories.CocktailRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

class GetAnimalsUseCase(
    private val cocktailRepo: CocktailRepo,
    private val scope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)
) {
    suspend fun getAnimals(animalType: AnimalType): List<Animal> {
        Log.e("HIII", "NOOOOO")
        return cocktailRepo.getAnimalsByType(animalType)
    }
}