package com.example.domain.usecases

import android.util.Log
import com.example.domain.models.Categories
import com.example.domain.models.Category
import com.example.domain.models.Drink
import com.example.domain.repositories.CocktailRepo
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.SupervisorJob
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
private const val TAG = "GetDrinksByCategoryUC"

class GetDrinksByCategoryUseCase (
    private val cocktailRepo: CocktailRepo,
    private val scope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)
) {
    suspend operator fun invoke(category: String): Flow<Result<List<Drink>>> = callbackFlow {
        Log.e(TAG, "HI MOM")
        val job = scope.launch {
            cocktailRepo.getDrinksByCategory(category)
                .catch { ex ->
                    send(Result.failure(ex))
                }.collect { drinks ->
                    Log.e(TAG, "GetDrinksUseCase -- The result from cocktailRepo is: ${drinks.size}")
                    send(Result.success(drinks))
                }
        }
        // "When everything is done in Producer scope, cancel the coroutine or else we'll have a memory leak" - Josh
        awaitClose { job.cancel() }
    }
}