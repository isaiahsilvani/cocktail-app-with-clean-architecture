package com.example.domain.usecases

import com.example.domain.repositories.CocktailRepo

class GetDrinkDetailsByIdUseCase (private val cocktailRepo: CocktailRepo) {
    suspend fun execute(id: String) = cocktailRepo.getDrinkDetailsById(id)
}