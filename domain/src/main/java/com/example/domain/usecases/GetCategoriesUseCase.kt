package com.example.domain.usecases

import android.util.Log
import com.example.domain.models.Categories
import com.example.domain.repositories.CocktailRepo
import kotlinx.coroutines.*
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collectLatest

private const val TAG = "GetCategoriesUseCase"

// Transform the DAO Category to the local model Category]
// Responsible for determining if something was or not available
class GetCategoriesUseCase(
    private val cocktailRepo: CocktailRepo,
    private val scope: CoroutineScope = CoroutineScope(SupervisorJob() + Dispatchers.IO)
) {

    suspend operator fun invoke(): Flow<Result<Categories>> = callbackFlow {
        val job = scope.launch {
            cocktailRepo.getCategories()
                .catch { ex ->
                    Log.e(TAG, "invoke: There was an error $ex")
                    send(Result.failure(ex))
                }.collectLatest { categories ->
                    send(Result.success(categories))
                }
        }
        // "When everything is done in Producer scope, cancel the coroutine or else we'll have a memory leak" - Josh
        awaitClose { job.cancel() }
    }
}