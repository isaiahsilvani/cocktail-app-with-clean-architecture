package com.example.domain.models

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(tableName = "animal_table")
data class Animal(
    @PrimaryKey
    val image: String,
    val type: AnimalType
)