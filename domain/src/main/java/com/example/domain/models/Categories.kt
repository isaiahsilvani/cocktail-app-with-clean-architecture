package com.example.domain.models

data class Categories(
    val drinks: List<Category>
)