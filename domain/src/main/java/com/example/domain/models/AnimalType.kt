package com.example.domain.models

enum class AnimalType {
    DOG, CAT, BIRD
}